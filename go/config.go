package main

type MateLightConfig struct {
	ControlPort string
	CrapPort    int
	CratesX     int
	CratesY     int
	CrateSizeX  int
	CrateSizeY  int
}

func NewConfig() *MateLightConfig {
	return &MateLightConfig{
		ControlPort: ":8081",
		CrapPort:    1337,
		CratesX:     8,
		CratesY:     4,
		CrateSizeX:  5,
		CrateSizeY:  4,
	}
}
