package main

import (
	"context"
	_ "embed"
	"errors"
	"fmt"
	"log"
	"net/http"
	"strings"
	"time"

	"nhooyr.io/websocket"
	"nhooyr.io/websocket/wsjson"
)

//go:embed monitor.html
var monitorHtml []byte

type monitorSender struct {
	frames    chan *[]byte
	closeSlow func()
}

// subscribe subscribes the given WebSocket to all broadcast messages.
// It creates a subscriber with a buffered msgs chan to give some room to slower
// connections and then registers the subscriber. It then listens for all messages
// and writes them to the WebSocket. If the context is cancelled or
// an error occurs, it returns and deletes the subscription.
//
// It uses CloseRead to keep reading from the connection to process control
// messages and cancel the context if the connection drops.
func (app *MateLightApp) subscribe(ctx context.Context, c *websocket.Conn) error {
	ctx = c.CloseRead(ctx)

	s := &monitorSender{
		frames: make(chan *[]byte, app.subscriberMessageBuffer),
		closeSlow: func() {
			c.Close(websocket.StatusPolicyViolation, "connection too slow to keep up with messages")
		},
	}
	app.addMonitor(s)
	defer app.deleteMonitor(s)

	for {
		select {
		case frame := <-s.frames:
			//fmt.Printf("DBG01: %v\n", frame)
			err := writeTimeout(ctx, time.Second*5, c, *frame)
			if err != nil {
				return err
			}
		case <-ctx.Done():
			return ctx.Err()
		}
	}
}

func (app *MateLightApp) monitorHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "GET, OPTIONS")

	switch r.Method {
	case "":
		fallthrough
	case "GET":
		// check for connection uprade header
		isUpgrade := false
		for _, s := range r.Header["Connection"] {
			for _, t := range strings.Split(s, ",") {
				if strings.ToLower(strings.TrimSpace(t)) == "upgrade" {
					isUpgrade = true
				}
			}
		}
		if isUpgrade {
			ws, err := websocket.Accept(w, r, &websocket.AcceptOptions{
				Subprotocols: []string{"matemon"},
			})
			if err != nil {
				fmt.Printf("Websocket Err: %v\n", err)
				return
			}
			defer ws.Close(websocket.StatusInternalError, "the sky is falling")

			if ws.Subprotocol() != "matemon" {
				ws.Close(websocket.StatusPolicyViolation, "client must speak the matemon subprotocol")
				return
			}

			jdata := struct {
				Screen struct {
					Crates_X    int
					Crates_Y    int
					CrateSize_X int
					CrateSize_Y int
				}
			}{
				Screen: struct {
					Crates_X    int
					Crates_Y    int
					CrateSize_X int
					CrateSize_Y int
				}{
					Crates_X:    8,
					Crates_Y:    4,
					CrateSize_X: 5,
					CrateSize_Y: 4,
				},
			}

			err = wsjson.Write(r.Context(), ws, jdata)
			if err != nil {
				log.Println(err)
				return
			}
			// send first frame now
			err = ws.Write(r.Context(), websocket.MessageBinary, app.ml.Frame[:])
			if err != nil {
				log.Println(err)
				return
			}
			// subscribe for auto frames
			err = app.subscribe(r.Context(), ws)
			if errors.Is(err, context.Canceled) {
				return
			}
			if websocket.CloseStatus(err) == websocket.StatusNormalClosure ||
				websocket.CloseStatus(err) == websocket.StatusGoingAway {
				return
			}

			if err != nil {
				fmt.Printf("%v\n", err)
				return
			}
		} else {
			// no upgrade, deliver monitor html
			w.WriteHeader(http.StatusOK)
			w.Write(monitorHtml)
		}
	case "OPTIONS":
		{
			w.WriteHeader(http.StatusNoContent)
		}
	default:
		w.WriteHeader(http.StatusMethodNotAllowed)
	}
}

// addSubscriber registers a subscriber.
func (app *MateLightApp) addMonitor(ms *monitorSender) {
	app.monitorsMu.Lock()
	app.monitors[ms] = struct{}{}
	app.monitorsMu.Unlock()
}

// deleteSubscriber deletes the given subscriber.
func (app *MateLightApp) deleteMonitor(ms *monitorSender) {
	app.monitorsMu.Lock()
	delete(app.monitors, ms)
	app.monitorsMu.Unlock()
}

func writeTimeout(ctx context.Context, timeout time.Duration, c *websocket.Conn, msg []byte) error {
	ctx, cancel := context.WithTimeout(ctx, timeout)
	defer cancel()
	return c.Write(ctx, websocket.MessageBinary, msg)
}
