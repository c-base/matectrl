package main

import (
	"math"
	"net/http"
	"os"
	"sync"

	"gitlab.com/c-base/matelight/matectl/matelight"
)

type MateLightApp struct {
	config     *MateLightConfig
	ctlServer  *http.Server
	stopChan   chan os.Signal
	ml         *matelight.Matelight
	monitorsMu sync.Mutex
	monitors   map[*monitorSender]struct{}
	// subscriberMessageBuffer controls the max number
	// of messages that can be queued for a subscriber
	// before it is kicked.
	//
	// Defaults to 16.
	subscriberMessageBuffer int
}

func (mla *MateLightApp) StartMateLight() error {
	mla.ml = matelight.NewMatelight(mla.config.CratesX, mla.config.CratesY)
	return mla.ml.Start()
}

func (mla *MateLightApp) applyGamma(b byte) byte {
	// original code: GAMMA_APPLY(c) ((uint8_t)roundf(powf((c/255.0F), GAMMA) * brightness * 255))
	return byte(math.Round(math.Pow((float64(b)/255.0), mla.ml.GetGamma()) * mla.ml.GetBrightness() * 255))
}

func (mla *MateLightApp) SendFrameDirect(buf []byte) {

	mbuf := buf[:1920]

	for i, b := range buf {
		buf[i] = mla.applyGamma(b)
	}

	mla.ml.SendFrameDirect(buf)

	mla.monitorsMu.Lock()
	defer mla.monitorsMu.Unlock()

	for ms := range mla.monitors {
		select {
		case ms.frames <- &mbuf:
		default:
			go ms.closeSlow()
		}
	}
}

func NewApp(config *MateLightConfig) *MateLightApp {
	return &MateLightApp{
		config:                  config,
		subscriberMessageBuffer: 16,
		monitors:                make(map[*monitorSender]struct{})}
}
