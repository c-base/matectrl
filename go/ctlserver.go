package main

import (
	"context"
	"encoding/binary"
	"fmt"
	"io"
	"math"
	"net/http"
	"strconv"
	"syscall"
	"time"
)

func helpHandler(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte(`
	Übelsischt über pferfügbare Endpointse:
	
	* /help
	* /stop
	GET     /hw         	returns the display hardware configuration
	GET/PUT /brightness   	get/sets the brightness 0..255
	GET/PUT /gamma		   	get/sets the gamma factor 0..(max) float64

	GET 	/settings		returns current matelight setings
	GET/PUT /pixel
	GET     /frame
	GET     /status
	GET     /monitor        simple monotor view
	`))
}

func sendCoffe(w http.ResponseWriter) {
	w.WriteHeader(418)
	w.Write([]byte("Get your coffee somewhere else."))
}

func (app *MateLightApp) settingsHandler(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "":
		fallthrough
	case "GET":
		w.WriteHeader(http.StatusOK)
		fmt.Fprintf(w, `{ "MateLightSettings": {
	"Brightnes": %.3f,
	"BrightnesDMX": %d
	}
}`, app.ml.GetBrightness(), app.ml.GetBrightnessDMX())
	default:
		w.WriteHeader(http.StatusMethodNotAllowed)
	}
}

func (app *MateLightApp) brightnessHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "GET, PUT, OPTIONS")
	switch r.Method {
	case "":
		fallthrough
	case "GET":
		retType := "text"
		values := r.URL.Query()
		if val, ok := values["type"]; ok {
			if len(val) == 1 {
				retType = val[0]
			}
		}
		switch retType {
		case "text":
			// no query params, send text
			fmt.Fprintf(w, "%3d", app.ml.GetBrightnessDMX())
		case "raw":
			w.Write([]byte{app.ml.GetBrightnessDMX()})
		default:
			fmt.Fprintf(w, "{ Brightness: %d }", app.ml.GetBrightnessDMX())
		}
	case "PUT":
		inType := "text"
		values := r.URL.Query()
		if val, ok := values["type"]; ok {
			if len(val) == 1 {
				inType = val[0]
			}
		}
		switch inType {
		case "text":
			s, err := io.ReadAll(r.Body)
			if err != nil {
				w.WriteHeader(http.StatusBadRequest)
				return
			}
			b, err := strconv.ParseInt(string(s), 10, 9)
			if err != nil || b < 0 || b > 255 {
				w.WriteHeader(http.StatusBadRequest)
				return
			}
			app.ml.SetBrightnessDMX(byte(b))
			w.WriteHeader(http.StatusNoContent)
			return
		case "raw":
			var b [1]byte
			_, err := r.Body.Read(b[:])
			if err != nil {
				w.WriteHeader(http.StatusBadRequest)
				return
			}
			app.ml.SetBrightnessDMX(b[0])
			w.WriteHeader(http.StatusNoContent)
			return
		}
	case "OPTIONS":
		{
			w.WriteHeader(http.StatusNoContent)
		}
	default:
		w.WriteHeader(http.StatusMethodNotAllowed)
	}
}

func (app *MateLightApp) gammaHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "GET, PUT, OPTIONS")
	switch r.Method {
	case "":
		fallthrough
	case "GET":
		retType := "text"
		values := r.URL.Query()
		if val, ok := values["type"]; ok {
			if len(val) == 1 {
				retType = val[0]
			}
		}
		switch retType {
		case "text":
			// no query params, send text
			fmt.Fprintf(w, "%.2f", app.ml.GetGamma())
		case "raw":
			var buf [8]byte
			binary.LittleEndian.PutUint64(buf[:], math.Float64bits(app.ml.GetGamma()))
			w.Write(buf[:])
		default:
			fmt.Fprintf(w, "{ Gamma: %.2f }", app.ml.GetGamma())
		}
	case "PUT":
		inType := "text"
		values := r.URL.Query()
		if val, ok := values["type"]; ok {
			if len(val) == 1 {
				inType = val[0]
			}
		}
		switch inType {
		case "text":
			s, err := io.ReadAll(r.Body)
			if err != nil {
				w.WriteHeader(http.StatusBadRequest)
				return
			}
			f, err := strconv.ParseFloat(string(s), 64)
			if err != nil {
				w.WriteHeader(http.StatusBadRequest)
				return
			}
			app.ml.SetGamma(f)
			w.WriteHeader(http.StatusNoContent)
			return
		case "raw":
			var b [8]byte
			n, err := r.Body.Read(b[:])
			if err != nil || n != 8 {
				w.WriteHeader(http.StatusBadRequest)
				return
			}
			app.ml.SetGamma(math.Float64frombits(binary.LittleEndian.Uint64(b[:])))
			w.WriteHeader(http.StatusNoContent)
			return
		}
	case "OPTIONS":
		{
			w.WriteHeader(http.StatusNoContent)
		}
	default:
		w.WriteHeader(http.StatusMethodNotAllowed)
	}
}

func (app *MateLightApp) configHandler(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "":
		fallthrough
	case "GET":
		w.WriteHeader(http.StatusOK)
		w.Write([]byte(`{ "MateLightHardwareConfig": {
	"Crates_X": 8,
	"Crates_Y": 4,
	"CrateSize_X": 5,
	"CrateSize_Y": 4
	}
}`))
	default:
		w.WriteHeader(http.StatusMethodNotAllowed)
	}
}

func (app *MateLightApp) stopHandler(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte(`
	Stopping.
	Bye.
`))
	app.stopChan <- syscall.SIGUSR2
}

func (app *MateLightApp) CreateAndRunCtl() error {
	mux := http.NewServeMux()
	mux.HandleFunc("/stop", app.stopHandler)
	mux.HandleFunc("/hw", app.configHandler)
	mux.HandleFunc("/brightness", app.brightnessHandler)
	mux.HandleFunc("/gamma", app.gammaHandler)
	mux.HandleFunc("/settings", app.settingsHandler)
	mux.HandleFunc("/monitor", app.monitorHandler)
	mux.HandleFunc("/help", helpHandler)
	mux.HandleFunc("/", helpHandler)

	app.ctlServer = &http.Server{Addr: app.config.ControlPort, Handler: mux}
	return app.ctlServer.ListenAndServe()
}

func (app *MateLightApp) ShutdownCtl() error {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	return app.ctlServer.Shutdown(ctx)
}

func (app *MateLightApp) StopCtl() error {
	return app.ctlServer.Close()
}
