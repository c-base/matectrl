package main

import (
	"fmt"
	"os"
	"os/signal"
	"syscall"
)

func main() {

	config := NewConfig()

	app := NewApp(config)

	// Setting up signal capturing
	app.stopChan = make(chan os.Signal, 1)
	signal.Notify(app.stopChan, os.Interrupt, syscall.SIGTERM)

	fmt.Println("Starting matelight (the usb thingy)…")
	if err := app.StartMateLight(); err != nil {
		// handle err
		fmt.Printf("MateLight err: %v\n", err)
	} else {
		fmt.Println("MateThingsy started.")
	}

	go func() {
		if err := app.CreateAndRunCrap(); err != nil {
			// handle err
			fmt.Printf("Crap err: %v\n", err)
		}
	}()

	go func() {
		if err := app.CreateAndRunCtl(); err != nil {
			// handle err
			fmt.Printf("Ctl err: %v\n", err)
			app.stopChan <- syscall.SIGUSR1
		}
	}()

	// Waiting for SIGINT (kill -2)
	sig := <-app.stopChan

	fmt.Printf("\nGot signal %s, shut down.\n", sig.String())

	if err := app.ShutdownCtl(); err != nil {
		// handle err
		fmt.Printf("Ctl shutdown err: %v\n", err)
	}
}
