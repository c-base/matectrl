package main

import (
	"fmt"
	"net"
)

func (app *MateLightApp) CreateAndRunCrap() error {
	conn, err := net.ListenUDP("udp", &net.UDPAddr{
		Port: app.config.CrapPort,
		IP:   net.ParseIP("0.0.0.0"),
	})
	if err != nil {
		return err
	}
	defer conn.Close()

	for {
		buf := make([]byte, app.ml.GetFrameSize()*3+4)
		n, addr, err := conn.ReadFromUDP(buf)
		if err != nil {
			return err
		}
		go app.serveCrapDatagram(conn, addr, buf, n)
	}
}

func (app *MateLightApp) serveCrapDatagram(conn net.PacketConn, addr net.Addr, buf []byte, n int) {
	// check size
	// framesize * 3 + 4   crap frame rgb  <- only this for now
	// framesize * 4 + 4   crap frame rgba
	// cratesize * 3 + 7   crap crate rgb
	// cratesize * 4 + 7   crap crate rgba
	// len 1..8?            variuos commands

	sizeCrapTypeCmd := 6 // 1 (opcode) + 1 (payload) + 4 (crc32)
	if n == sizeCrapTypeCmd {
		if buf[0] == 0x02 {
			app.ml.SetBrightnessDMX(buf[1])
		}
	}

	sizeCrapType1np := app.ml.GetFrameSize() * 3
	sizeCrapType1 := sizeCrapType1np + 4
	if n != sizeCrapType1 && n != sizeCrapType1np {
		fmt.Printf("UDP ERR: Got Datagram %d bytes, expected %d (with crc) or %d (without crc) bytes.\n", n, sizeCrapType1, sizeCrapType1np)
		return
	}
	//fmt.Printf("UDP: Got Datagram %d bytes\n", n)
	app.SendFrameDirect(buf)
}
