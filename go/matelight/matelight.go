package matelight

type Matelight struct {
	usbmatelight *USBMatelight
	crates_x     int
	crates_y     int
	Frame        [1920]byte
	currentFrame *[]byte
	brightness   float64
	gamma        float64
}

func (ml *Matelight) SetBrightness(b float64) {
	ml.brightness = b
}

func (ml *Matelight) SetBrightnessDMX(b byte) {
	ml.brightness = float64(b) / 255.0
}

func (ml *Matelight) GetBrightness() float64 {
	return ml.brightness
}

func (ml *Matelight) GetBrightnessDMX() byte {
	return byte(ml.brightness * 255)
}

func (ml *Matelight) SetGamma(b float64) {
	ml.gamma = b
}

func (ml *Matelight) GetGamma() float64 {
	return ml.gamma
}

func (ml *Matelight) Start() error {
	ml.usbmatelight = &USBMatelight{}
	return ml.usbmatelight.Start()
}

func (ml *Matelight) Stop() {
	ml.usbmatelight.Destroy()
}

func (ml *Matelight) SendFrame() {
	ml.usbmatelight.SendFrame(ml.Frame[:], ml.crates_x, ml.crates_y)
}

func (ml *Matelight) SendFrameDirect(buf []byte) {
	ml.usbmatelight.SendFrame(buf, ml.crates_x, ml.crates_y)
}

// return the frame pixel count
func (ml *Matelight) GetFrameSize() int {
	return 20 * ml.crates_x * ml.crates_y
}

func NewMatelight(crates_x int, crates_y int) *Matelight {
	ml := &Matelight{}
	ml.crates_x = crates_x
	ml.crates_y = crates_y
	ml.brightness = 0.7
	ml.gamma = 2.5
	return ml
}
