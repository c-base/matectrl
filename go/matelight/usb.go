package matelight

// #cgo CFLAGS: -I -L${SRCDIR}/../../host -g -Wall -std=gnu11
// #cgo LDFLAGS: -L${SRCDIR}/../../host -lml -lm -lusb-1.0
// #include "../../host/usb.h"
import "C"
import (
	"errors"
	"fmt"
	"unsafe"
)

type USBMatelight struct {
	handle *C.struct_matelight_handle
}

func (uml *USBMatelight) Start() error {
	hu := C.matelight_usb_init()

	if hu != 0 {
		fmt.Printf("USB lib ret code: %d\n", hu)
		return errors.New("Cannot initialize USB library.")
	}

	uml.handle = C.matelight_open(nil)

	if uml.handle == nil {
		return errors.New("Cannot find a MateLight.")
	}
	return nil
}

func (uml *USBMatelight) Destroy() {
	C.matelight_usb_destroy()
}

func (uml *USBMatelight) SendFrame(buf []byte, crates_x int, crates_y int) {
	if uml.handle == nil {
		return
	}
	rc := C.matelight_send_frame(uml.handle, unsafe.Pointer(&buf[0]), C.size_t(crates_x), C.size_t(crates_y))
	if rc != 0 {
		panic("usb transfer error")
	}
}
