module gitlab.com/c-base/matelight/matectl

go 1.19

require nhooyr.io/websocket v1.8.7

require github.com/klauspost/compress v1.16.7 // indirect
